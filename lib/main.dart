import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Fikri First Calculator',
      theme: ThemeData(
          primarySwatch: Colors.blue,
          visualDensity: VisualDensity.adaptivePlatformDensity),
      home: MyHomePage(),
      debugShowCheckedModeBanner: true,
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int first, second; // Two values for calculate + - x /
  String opp;
  String result, text = "";

  void btnClicked(String btnText) {
    if (btnText == "C") {
      print("case 1");
      // Reset all value
      result = "";
      text = "";
      first = 0;
      second = 0;
    } else if (btnText == "+" ||
        btnText == "-" ||
        btnText == "*" ||
        btnText == "/") {
      print("case 2");
      // Save value first
      first = int.parse(text);
      result = "";
      opp = btnText;
    } else if (btnText == "=") {
      print("case 3");
      // Calculate value first and second
      second = int.parse(text);
      if (opp == "+") {
        result = (first + second).toString();
      } else if (opp == "-") {
        result = (first - second).toString();
      } else if (opp == "*") {
        result = (first * second).toString();
      } else if (opp == "/") {
        result = (first ~/ second).toString();
      }
    } else {
      print("case 4");
      result = int.parse(text + btnText).toString();
    }

    setState(() {
      text = result;
    });
  }

  Widget customOutlinedButton(String value) {
    return Expanded(
      child: OutlinedButton(
        onPressed: () => btnClicked(value),
        child: Text(
          value,
          style: TextStyle(fontSize: 25),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Text('Fikri First Calculator')),
        body: Container(
          child: Column(
            children: [
              Expanded(
                child: Container(
                  color: Colors.amber,
                  padding: EdgeInsets.all(10),
                  alignment: Alignment.center,
                  child: Text(
                    text,
                    style: TextStyle(
                        fontSize: 50,
                        fontWeight: FontWeight.w600,
                        color: Colors.red),
                  ),
                ),
              ),
              Row(
                children: [
                  customOutlinedButton("9"),
                  customOutlinedButton("8"),
                  customOutlinedButton("7"),
                  customOutlinedButton("+")
                ],
              ),
              Row(
                children: [
                  customOutlinedButton("6"),
                  customOutlinedButton("5"),
                  customOutlinedButton("4"),
                  customOutlinedButton("-")
                ],
              ),
              Row(
                children: [
                  customOutlinedButton("3"),
                  customOutlinedButton("2"),
                  customOutlinedButton("1"),
                  customOutlinedButton("*")
                ],
              ),
              Row(
                children: [
                  customOutlinedButton("C"),
                  customOutlinedButton("0"),
                  customOutlinedButton("="),
                  customOutlinedButton("/")
                ],
              )
            ],
          ),
        ));
  }
}
